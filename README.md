Talks
=============

Slides based on [AccesSlide](https://github.com/access42/AccesSlide), a framework to create accessible presentations with HTML5-CSS3-JS, using the [ffoodd fork](https://github.com/ffoodd/Talks).
